<?php
require_once "db.php";
require "checkPermission.php";
if(checkLogin()) {
    header('Location: mainPage.php');
    die;
}
include('navbar.php');
?>
<html>

<head>
    <title>Logowanie</title>
    <link type="text/css" rel="stylesheet" href="style.css" />
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</head>

<body>

<h3>Strona Logowania</h3>
<div class="container">

        <form action="login.php" method="POST">
            <label>Login:</label>
            <p>
                 <input type="text" name="login" required/><br>
            </p>
             <label>Hasło:</label>
            <p>
                 <input type="password" name="password" required/><br>
             </p>
             <input type="submit" class="btn btn-success my-2 my-sm-0" value="Zaloguj"/>

        </form>
    <?php if(!empty($_SESSION['error']))	echo $_SESSION['error']; unset($_SESSION['error']); ?>
</div>
</body>

</html>
