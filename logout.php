<?php
require "checkPermission.php";
if(!checkLogin()) {
    header('Location: loginPage.php');
    die;
}
unset($_SESSION['user']);
header('Location:loginPage.php');
die;
?>