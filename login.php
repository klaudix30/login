<?php
require_once "db.php";
require "checkPermission.php";
if(checkLogin()) {
    header('Location: mainPage.php');
    die;
}

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    $login = trim($_POST['login']);
    $password = trim($_POST['password']);
    $query = "SELECT * from `users` where  login='$login'";
    $result = $db->query($query);

    if (mysqli_num_rows($result) == 1) {
        $row = mysqli_fetch_assoc($result);
        if (password_verify($password, $row['password'])) {
            $_SESSION['user'] = array(
                'login' => $_POST['login'],
                'password' => $_POST['password']
            );
            header('Location: mainPage.php');
            die;
        }else {
            $_SESSION['error'] = '<div class="error message">Blędne dane logowania</div>';

        }

    }else{
        $_SESSION['error'] = '<div class="error message">Blędne dane logowania</div>';
    }
}
header('Location: loginPage.php');
die;

