<?php
require_once "db.php";
include "checkPermission.php";
if(!checkLogin()) {
    header('Location: loginPage.php');
    die;
}else{
    if(!checkAdmin($db)){
        header('Location: mainPage.php');
    }
}
include('navbar.php');

$query='SELECT * from users where login <> "'.$_SESSION['user']['login'] .'"';
$result=$db->query($query);
?>

<html>

<head>
    <title>Strona glowna</title>
    <link type="text/css" rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</head>

<body>
<div class="container">
    <table class="table my-2 my-sm-0">
        <thead class="thead-dark">
        <tr>
            <th scope="col"></th>
            <th scope="col">login</th>
            <th scope="col">email</th>
            <th scope="col">wiek</th>
            <th scope="col">telefon</th>
            <th scope="col">miasto</th>
            <th scope="col">akcje</th>
        </tr>
        </thead>
        <tbody>
        <?php  while ($row=mysqli_fetch_assoc($result)): ?>
        <tr>
            <th scope="row"></th>
            <td><?php echo $row['login'];?></td>
            <td><?php echo $row['email'];?></td>
            <td><?php echo $row['age'];?></td>
            <td><?php echo $row['phone'];?></td>
            <td><?php echo $row['city'];?></td>
            <?php if($row['permissions']==1): ?>
            <td>
                <a class="btn btn-success my-2 my-sm-0" href="user.php?login=<?=$row['login']?>">edytuj</a>
                <a class="btn btn-success my-2 my-sm-0" href="deleteUser.php?login=<?=$row['login']?>">usun</a>
            </td>
            <?php else: ?>
                <td>
                    <p>Nie można nic zrobić</p>
                </td>
            <?php endif ?>
        </tr>
        <?php endwhile ?>
        </tbody>
    </table>
</div>
</body>

</html>
