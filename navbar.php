

<nav class="navbar navbar-expand-lg navbar-light bg-secondary" style="margin-bottom: 40px;">
    <a class="navbar-brand" href="#">Stronka</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="#"></a>
            </li>
        </ul>
    <?php if(!checkLogin()){?>
        <form class="form-inline my-2 my-lg-0">
            <a class="btn btn-success my-2 my-sm-0 mr-1" href="loginPage.php">Logowanie</a>
            <a class="btn btn-success my-2 my-sm-0" href="registerPage.php">Rejestracja</a>
        </form>
    <?php }elseif(!checkAdmin($db)){?>
    <ul class="navbar-nav my-2 my-lg-0 mr-3">
        <li class="nav-item dropdown my-2 my-lg-0">
            <div class="dropdown">
                <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Moje konto
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="user.php?login=<?=$_SESSION['user']['login']?>">edytuj dane</a>
                    <a class="dropdown-item" href="deleteUser.php?login=<?=$_SESSION['user']['login']?>" onclick="confirm('aa')">usun konto</a>
                    <a class="dropdown-item" href="logout.php">wyloguj</a>
                </div>
            </div>
        </li>
    </ul>
    <?php }else{?>

        <ul class="navbar-nav my-2 my-lg-0 mr-3">
            <li class="nav-item dropdown my-2 my-lg-0">
                <div class="dropdown">
                    <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Moje konto
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="user.php?login=<?=$_SESSION['user']['login']?>">edytuj dane</a>
                        <a class="dropdown-item" href="deleteUser.php?login=<?=$_SESSION['user']['login']?>">usun konto</a>
                        <a class="dropdown-item" href="adminPanel.php">Lista użytkowników</a>
                        <a class="dropdown-item" href="logout.php">wyloguj</a>
                    </div>
                </div>
            </li>
        </ul>

    <?php }?>
</nav>


