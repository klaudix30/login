<?php

require_once "db.php";
require "checkPermission.php";
if(!checkLogin()) {
    header('Location: loginPage.php');
    die;
}

$login=$_GET["login"];
$query = "SELECT * from `users` where login='$login'";
$result=$db->query($query);
if($result){
    $user=$result->fetch_assoc();
    //var_dump($user);
}else{
    echo 'taki uzytkownik nie istnieje';
    die;
}
include('navbar.php');
?>

<html>

<head>
    <title>edycja</title>
    <link type="text/css" rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</head>

<body>
<h3>edycja</h3>
<div class="container">

    <form action="editUser.php" method="POST">
        <label>Login:</label>
        <p>
            <input type="text" name="login" value="<?php echo $user['login']; ?>"><br>
        </p>
        <label>email:</label>
        <p>
            <input type="text" name="email" value="<?php echo $user['email']; ?>"><br>
        </p>
        <label>wiek:</label>
        <p>
            <input type="text" name="age" value="<?php echo $user['age']; ?>"><br>
        </p>
        <label>telefon:</label>
        <p>
            <input type="text" name="phone" value="<?php echo $user['phone']; ?>"><br>
        </p>
        <label>miejscowosc:</label>
        <p>
            <input type="text" name="city" value="<?php echo $user['city']; ?>"><br>
        </p>
        <input type="submit" value="edytuj"/>

    </form>

</div>
</body>

</html>